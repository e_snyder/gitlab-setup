provider google {
  version = "~> 2.3"
  region  = "${var.project_region}"
  zone    = "${var.project_zone}"
}

provider google-beta {
  version = "~> 2.3"
  region  = "${var.project_region}"
  zone    = "${var.project_zone}"
}

provider random {
  version = "~> 2.1"
}

resource "random_pet" "project_random_id" {
  prefix = "es-gitlab-demo"
}

resource "google_project" "gitlab_project" {
  name                = "Gitlab Demo"
  project_id          = "${random_pet.project_random_id.id}"
  folder_id           = "${var.project_folder}"
  billing_account     = "${var.project_billing}"
  auto_create_network = false
}

resource "google_project_service" "compute_api_service" {
  project = "${google_project.gitlab_project.id}"
  service = "compute.googleapis.com"
}

resource "google_project_service" "sql_api_service" {
  project = "${google_project.gitlab_project.id}"
  service = "sqladmin.googleapis.com"
}

resource "google_project_service" "servicenetworking_api_service" {
  project = "${google_project.gitlab_project.id}"
  service = "servicenetworking.googleapis.com"
}

resource "google_project_service" "container_api_service" {
  project = "${google_project.gitlab_project.id}"
  service = "container.googleapis.com"
}

resource "google_project_service" "dns_api_service" {
  project = "${google_project.gitlab_project.id}"
  service = "dns.googleapis.com"
}

resource "google_project_service" "logging_api_service" {
  project = "${google_project.gitlab_project.id}"
  service = "logging.googleapis.com"
}

resource "google_project_service" "monitoring_api_service" {
  project = "${google_project.gitlab_project.id}"
  service = "monitoring.googleapis.com"
}

resource "google_project_service" "stackdriver_api_service" {
  project = "${google_project.gitlab_project.id}"
  service = "stackdriver.googleapis.com"
}
