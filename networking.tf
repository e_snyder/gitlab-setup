resource "google_compute_network" "gitlab_network" {
  project = "${google_project.gitlab_project.id}"
  name    = "gitlab-network"
}

resource "google_compute_global_address" "shared_services_range" {
  provider      = "google-beta"
  project       = "${google_project.gitlab_project.id}"
  name          = "shared-services-global-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = "${google_compute_network.gitlab_network.self_link}"
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider                = "google-beta"
  network                 = "${google_compute_network.gitlab_network.self_link}"
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = ["${google_compute_global_address.shared_services_range.name}"]
}

resource "google_compute_address" "gitlab_address" {
  name    = "gitlab-ip"
  project = "${google_project.gitlab_project.id}"
}

resource "google_dns_managed_zone" "gitlab_zone" {
  name        = "mygitlab-zone"
  project     = "${google_project.gitlab_project.id}"
  dns_name    = "${var.dns_zone_name}."
  description = "Gitlab Zone"
}

resource "google_dns_record_set" "gitlab_primary_dns" {
  name    = "gitlab.${google_dns_managed_zone.gitlab_zone.dns_name}"
  project = "${google_project.gitlab_project.id}"
  type    = "A"
  ttl     = "300"

  managed_zone = "${google_dns_managed_zone.gitlab_zone.name}"

  rrdatas = ["${google_compute_address.gitlab_address.address}"]
}

resource "google_dns_record_set" "gitlab_registry_dns" {
  name    = "registry.${google_dns_managed_zone.gitlab_zone.dns_name}"
  project = "${google_project.gitlab_project.id}"
  type    = "A"
  ttl     = "300"

  managed_zone = "${google_dns_managed_zone.gitlab_zone.name}"

  rrdatas = ["${google_compute_address.gitlab_address.address}"]
}

resource "google_dns_record_set" "minio_registry_dns" {
  name    = "minio.${google_dns_managed_zone.gitlab_zone.dns_name}"
  project = "${google_project.gitlab_project.id}"
  type    = "A"
  ttl     = "300"

  managed_zone = "${google_dns_managed_zone.gitlab_zone.name}"

  rrdatas = ["${google_compute_address.gitlab_address.address}"]
}
