# gitlab-setup

This repository exists as a demonstration of a Gitlab deployment to Kubernetes.

It was demonstrated once and has been minimally updated. The updates resolve a bug with the database password handling and moves the certmanager issuer into a variable.

You can read all the gory details of the result of the demonstration here: https://medium.com/@elijah.snyder/interviewing-at-gitlab-66539aeb28c0

--------------

## Implementation details

Google Container Engine (GKE) is the target installation environment. The installation is handled by [terraform](https://www.terraform.io/). We want to leverage newer features of Google Cloud Platform (GCP) to enable the usage of Google Cloud SQL as the Gitlab PostgreSQL database backend. We will also use Cloud DNS as a delegation target for our domain. Finally, we will use the [Gitlab Helm Chart](https://docs.gitlab.com/charts/) to install Gitlab Enterprise Edition.

### Cloud SQL

#### VPC Native features on Google Cloud Platform

As of March 31st, 2019, all Kubernetes clusters in GKE default to VPC Native. This networking option allows better integration with services across GCP.

Since our Kubernetes cluster is new and features VPC Native networking, we can leverage the Service Networking API to automatically connect our SQL instance to the Kubernetes cluster. The removal of the unnecessary public access allows for security and traffic benefits: actors must be within the VPC network to access the database and all of our database traffic is no longer driven through public routes. The approach is more secure, reduces ingress/egress costs, and cuts our latency compared to previous Cloud SQL usage via proxies and public IPs.

#### Features

Cloud SQL features include HA, read replicas, and automatic maintenance. We can use this as a persistent database if we want to reinstall or move Gitlab to a new location later. Backups and upgrades are automatic.

### DNS

I use [Google Domains](https://domains.google.com) for personal sites. Unfortunately, there doesn't seem to be any sort of API for automated management.

Instead, we'll create a domain and delegate to [Cloud DNS](https://cloud.google.com/dns/docs/). Terraform will build our Cloud SQL Managed Zone and add appropriate records for our installation.

Individual DNS entries are created and managed through terraform rather than relying on a wildcard entry pointed at Gitlab.

### Gitlab Helm Chart

#### Experience

I have not used the Gitlab Helm Chart previously, but have used many other charts. The experience was far beyond expectations with Gitlab including DNS management and much, much more. The chart itself orchestrates certificates, database migrations, and creating external endpoints. To be honest, when I first approached the charts, I had already started crafting configuration for the nginx ingress controller to add public IPs - but to my surprise, a shortcut for setting the load balancer IP was wrapped in the chart itself.

## Run Book

You will need to supply a `database_secret` that is used to create the Cloud SQL user and is later added to a secret for Gitlab to consume. The `.gitignore` in this repository ignores tfvars: use the autoload feature of terraform and add your secrets to terraform.tfvars. `project_billing` and `project_folder` also need to be supplied to create the Kubernetes cluster. `dns_zone_name` is the domain you want Gitlab to become a subdomain of. Finally, a `certmanager_issuer` needs to be a valid certificate to complete certificate provisioning.

### Terraform Variables

| Variable | Default Value |
| -------- | ------------- |
| `project_region` | us-central1 |
| `project_zone` | us-central1-a |
| `kubernetes_node_count` | 4 |
| `kubernetes_node_preempt` | true |
| `kubernetes_node_type` | n1-standard-4 |

### A warning on resource timeouts

DNS reconfiguration, automatic certificate generation, and API management can take time during initial provisioning. You may need to run terraform more than once to complete the full setup.

### Sanity Checks, trouble shooting

gitlab.<dns_zone_name>, registry.<dns_zone_name>, and minio.<dns_zone_name> should all be resolvable.

If you receive a certificate error when visiting the Gitlab installation and the certificate is the nginx default "Fake Certificate" you may want to check for Ingress objects in Kubernetes with ACME related (ie: .well-known/acme-challenge) urls. They may have not yet resolved their challenges yet.

### Known Issues

Gitlab Pages and Gitlab Geo are currently not supported with the Kubernetes chart. These features may not work if enabled.

This installation uses *preemptible nodes* and may be subject to service disruption.
