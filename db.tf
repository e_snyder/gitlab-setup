resource "google_sql_database_instance" "gitlab_db" {
  name             = "gitlab-db-micro"
  database_version = "POSTGRES_9_6"
  project          = "${google_project.gitlab_project.id}"

  settings {
    tier = "db-f1-micro"

    ip_configuration {
      ipv4_enabled    = false
      private_network = "${google_compute_network.gitlab_network.self_link}"
    }

    maintenance_window {
      day  = 1
      hour = 0
    }
  }

  depends_on = [
    "google_service_networking_connection.private_vpc_connection",
  ]
}

resource "google_sql_user" "gitlab_sql_user" {
  name     = "gitlab"
  instance = "${google_sql_database_instance.gitlab_db.id}"
  project  = "${google_project.gitlab_project.id}"
  password = "${var.database_secret}"
}

resource "google_sql_database" "gitlab_database" {
  name      = "gitlab"
  instance  = "${google_sql_database_instance.gitlab_db.id}"
  project   = "${google_project.gitlab_project.id}"
  charset   = "UTF8"
  collation = "en_US.UTF8"
}
